<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

if(isset($_POST["submit"])){ 

$client = new Google_Client();
$client->setAuthConfigFile('client_secret.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
 
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'q5: '.$_POST['description'],
  'location' => 'Jerusalem',
  'description' => 'Testing the google calendar API',
  'start' => array(
    'dateTime' => $_POST['date'].'T'.$_POST['starttime'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => $_POST['date'].'T'.$_POST['endtime'].':00+02:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
  //'RRULE:FREQ=DAILY;COUNT=1'
  ),
  'attendees' => array(
    array('email' => 'lpage@example.com'),
    array('email' => 'exm@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));

	$calendarId = 'primary';
	$event = $service->events->insert($calendarId, $event);
	printf('Event created: %s\n', $event->htmlLink);
 }
else {
  $redirect_uri =  'http://orya.myweb.jce.ac.il/calender/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

}

?>

<html lang="he">
<head>
<title>Calendar form</title>
<meta charset="utf-8">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body class="body">
<form method="POST" action="">
<header>
<h2>Calandar Form</h2>
</header>
<div class="form-group">
<label for="description">Description:</label>
<input type="text" name="description" class="form-control" id="description" required> <!--onfocus="myfocusfunction(this)" onblur="myblurfunction(this)"-->
</div>
<div class="form-group">
<label for="date">Date:</label>
<input type="date" name="date" class="form-control" id="date" required> <!--onfocus="myfocusfunction(this)" onblur="myblurfunction(this)"-->
</div>
<div class="form-group">
<label for="time">Start time</label>
<input type="time" name="starttime" class="form-control" id="starttime" required> <!--onfocus="myfocusfunction(this)" onblur="myblurfunction(this)"-->
</div>
<div class="form-group">
<label for="time">End time</label>
<input type="time" name="endtime" class="form-control" id="endtime" required> <!--onfocus="myfocusfunction(this)" onblur="myblurfunction(this)"-->
</div>

<input type="submit" value=" Submit " name="submit"/>
</form>

</body>

</html>


